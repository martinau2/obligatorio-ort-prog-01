window.addEventListener("load",inicio);


let DocentesRegistrados = [] //lista que mantiene los docentes registrados
let AlumnosRegistrados = [] //lista que mantiene los alumnos registrados

let UsuarioActual = null; //usuario que esta logueado en este momento

let error = ""; //guarda el msj de error de las funciones

let ContadorEntregas=1 //mantiene la cuenta de las entregas y lo utiliza para identificarlas 

function inicio()
{
    document.querySelector("#btnLogin").addEventListener("click",DivMostrarLogin)

    document.querySelector("#btnRegistro").addEventListener("click",DivMostrarRegistrar)

    document.querySelector("#btnDocente").addEventListener("click",DivMostrarRegistroDocente)

    document.querySelector("#btnAlumno").addEventListener("click",DivMostrarRegistroAlumno)

    document.querySelector("#btnDRegistro").addEventListener("click",RegistrarDocente)

    document.querySelector("#btnARegistro").addEventListener("click",RegistrarAlumno)

    document.querySelector("#btnIngreso").addEventListener("click",IngresarUsuario)
    
    document.querySelector("#btnSubirEJ").addEventListener("click",DivMostrarSubirEJ)
    document.querySelector("#btnIngresarTarea").addEventListener("click",SubirTarea)

    document.querySelector("#BotonMostrarNivel").addEventListener("click",DivMostrarNivelDocente)
    document.querySelector("#BotonSubirNivel").addEventListener("click",SubirDeNivelaAlumnos)

    document.querySelector("#btnVerEj").addEventListener("click",DivVerTareasAlumno)
    document.querySelector("#btnBuscarTarea").addEventListener("click",BuscarTarea)
    document.querySelector("#verSeleccionarTarea").addEventListener("change",MostrarTareas)
 
    document.querySelector("#btnEntregas").addEventListener("click",DivMostrarEntregaAlumno)
    document.querySelector("#btnSubirEntrega").addEventListener("click",EntregarTareas)

    document.querySelector("#btnDevoluciones").addEventListener("click",DivMostrarDevolucionesDocente)
    document.querySelector("#BotonCorregirTarea").addEventListener("click",HacerDevolucionEntrega)
    document.querySelector("#SeleccionarTareas").addEventListener("change",CambiarAudioEntregas)

    document.querySelector("#BotonVerDevoluciones").addEventListener("click",MostrarDivDevolucionDocente)
    document.querySelector("#VerDevolucionDocente").addEventListener("click",VerDevolucionDocente)

    document.querySelector("#BotonMostrarDEstadisticas").addEventListener("click",EstadisticasDocente)
    document.querySelector("#btnAEstadisticas").addEventListener("click",EstadisticasAlumno)
    document.querySelector("#BotonElegirA").addEventListener("click",MostrarEstadisticasAlumno)

    document.querySelector("#BotonTareasResueltas").addEventListener("click",MostrarDivTareasResueltas)

    document.querySelector("#btnSalir").addEventListener("click",Salir)
    document.querySelector("#btnSalir2").addEventListener("click",Salir)

    document.querySelector("#inputImagen").addEventListener("change",MostrarIMG)

    OcultarDivsMenuInicial()
    OcultarDivsInterfazAlumno()
    OcultarDivsInterfazDocente()
    
    //Datos precargados

    DocentesRegistrados.push(new Docente("Martin Arbe","Martin1","martin")) //Precargo a un docente
    DocentesRegistrados.push(new Docente("caro de los santos","caro1","caro")) //Precargo otro docente
    
    AlumnosRegistrados.push(new Alumno("Gerardito B.","Gera1","gera",DocentesRegistrados[0])) //Precardo un alumno
    AlumnosRegistrados[0].docente.alumnos.push(AlumnosRegistrados[0]); //busco mi docente y me agrego a mi mismo

    DocentesRegistrados[0].tareas.push(new Tarea("inicial","ej1.png","Esta es la descripcion","Este es el titulo"))//agrego una tarea al docente 0 (martin)
    DocentesRegistrados[0].tareas.push(new Tarea("inicial","ej2.png","Segunda tarea de prueba","Ej2 de prueba"))
}

// ZONA FUNCIONES QUE OCULTAN O MUESTRAN DIVS

//#region Funciones que ocultan divs
function OcultarDivsMenuInicial()
{
    document.querySelector("#login").style.display = "none";
    document.querySelector("#registro").style.display = "none";
    document.querySelector("#registroAlumno").style.display = "none";
    document.querySelector("#registroDocente").style.display = "none";
    document.querySelector("#divMenuDocente").style.display = "none";
    document.querySelector("#divMenuAlumno").style.display = "none";
}

function OcultarDivsInterfazAlumno()
{
    document.querySelector("#EntregarTarea").style.display = "none";
    document.querySelector("#VerTarea").style.display = "none";
    document.querySelector("#VerDevoluciondeDocente").style.display = "none";
    document.querySelector("#VerTareasResueltas").style.display = "none";
    document.querySelector("#EstadisticasA").style.display = "none";
}

function OcultarDivsInterfazDocente()
{
    document.querySelector("#subirEJ").style.display = "none";
    document.querySelector("#SubirNivel").style.display="none";
    document.querySelector("#DevolucionesTareas").style.display="none";
    document.querySelector("#EstadisticasD").style.display = "none";
}

//#endregion

//#region Funciones que muestran divs

function DivMostrarLogin()
{
    OcultarDivsMenuInicial()
    document.querySelector("#login").style.display = "block";
}

function DivMostrarRegistrar()
{
    OcultarDivsMenuInicial()
    document.querySelector("#registro").style.display = "block";
}

function DivMostrarRegistroDocente()
{
    document.querySelector("#registroAlumno").style.display = "none";
    document.querySelector("#registroDocente").style.display = "block";
}



function DivMostrarRegistroAlumno()
{
    document.querySelector("#registroDocente").style.display = "none";
    document.querySelector("#registroAlumno").style.display = "block";
    document.querySelector("#inputADocente").innerHTML="";

    for(const Docente of DocentesRegistrados){
        document.querySelector("#inputADocente").innerHTML+= "<Option value="+Docente.usuario+">"+Docente.usuario+"</option>"
        // le pongo un + poruqe voy agregando opciones
        //lo que va aca tiene que ser string y lo hago con comillas 
    }
    
}

function DivMostrarSubirEJ()
{
    OcultarDivsInterfazDocente()
    document.querySelector("#subirEJ").style.display = "block";
}

function DivMostrarNivelDocente(){
    OcultarDivsInterfazDocente()
    document.querySelector("#SubirNivel").style.display="block";
    document.querySelector("#SeleccionarNivel").innerHTML=""; //limpio el select antes de crearlo
    for (const Alumno of UsuarioActual.alumnos){ //Para cada alumno del docente actual
             document.querySelector("#SeleccionarNivel").innerHTML+="<Option value="+Alumno.usuario+">"+Alumno.nombre+"-"+Alumno.nivel+"</option>"
    }

    if(UsuarioActual.alumnos.length == 0)
    {
        document.querySelector("#DevolucionNivel").innerHTML="No hay alumnos registrados a nombre de este docente"
    }
}

function DivVerTareasAlumno()
{
    let tareas = 0; //contador de tareas para saber si hay tareas para entregar

    OcultarDivsInterfazAlumno()
    document.querySelector("#VerTarea").style.display = "block";

    document.querySelector("#listaBuscarTarea").innerHTML = "";
    document.querySelector("#inputBuscarTarea").value = "";

    document.querySelector("#verDevolucion").innerHTML = ""
    document.querySelector("#verSeleccionarTarea").innerHTML="";
    document.querySelector("#verEntregaTituloMostrar").innerHTML = ""; //limpio el titulo 
    document.querySelector("#verDescripcionMostrar").innerHTML = ""; //limpio la descripcion
    document.querySelector("#verImagenMostrar").src = ""; //limpio la imagen

    for (const tarea of UsuarioActual.docente.tareas){ //Recorro cada tarea del docente del alumno actual

        if (UsuarioActual.nivel==tarea.nivel){ //Si el nivel de la tarea corresponde al nivel del alumno

            let fueentregada = false; //fue entregado? falso

            for (const entrega of UsuarioActual.entregas) { //por cada entrega que el usuario actual hizo
                if (entrega.tareaOriginal == tarea) //si esta entrega ya es de la tarea que estoy viendo
                {
                    fueentregada = true; //esa tarea ya fue entregada
                }
            }
            if (fueentregada == false) // si esa tarea no fue entregada: añado la tarea a la lista de posibles tareas para hacer
            {
                document.querySelector("#verSeleccionarTarea").innerHTML+="<Option value=\""+tarea.titulo+"\">"+tarea.titulo+"</option>"
                tareas++; //Solamente hay tareas si corresponde al nivel del alumno actual y no fue entregada
            }
            
        }
    }

    if(tareas > 0) //si conte almenos 1 tarea:
    {
        MostrarTareas(); //muestro la tarea
        document.querySelector("#verImagenMostrar").style.display = "block"
        document.querySelector("#buscadorTareas").style.display = "block"
        
    }
    else 
    {
        document.querySelector("#verDevolucion").innerHTML = "No hay tareas que mostrar" //devuelvo que no hay tareas
        document.querySelector("#buscadorTareas").style.display = "none"
        document.querySelector("#verImagenMostrar").style.display = "none"
    }
}

function DivMostrarEntregaAlumno(){
    let tareas = 0; //contador de tareas para saber si hay tareas para entregar

    OcultarDivsInterfazAlumno()
    document.querySelector("#EntregarTarea").style.display = "block";

    document.querySelector("#SeleccionarTarea").innerHTML="";
    document.querySelector("#devolucionEntregarTarea").innerHTML = ""

    for (const tarea of UsuarioActual.docente.tareas){ //Recorro cada tarea del docente del alumno actual

        if (UsuarioActual.nivel==tarea.nivel){ //Si el nivel de la tarea corresponde al nivel del alumno

            let fueentregada = false; //fue entregado? falso

            for (const entrega of UsuarioActual.entregas) { //por cada entrega que el usuario actual hizo
                if (entrega.tareaOriginal == tarea) //si esta entrega ya es de la tarea que estoy viendo
                {
                    fueentregada = true; //esa tarea ya fue entregada
                }
            }
            if (fueentregada == false) // si esa tarea no fue entregada: añado la tarea a la lista de posibles tareas para hacer
            {
                document.querySelector("#SeleccionarTarea").innerHTML+="<Option value=\""+tarea.titulo+"\">"+tarea.titulo+"</option>"
                tareas++; //Solamente hay tareas si corresponde al nivel del alumno actual y no fue entregada
            }
            
        }
    }

    if (tareas > 0) //si logre contar almenos una tarea que no haya entregado o sea del nivel del alumno muestro esa tarea
    {
        document.querySelector("#btnSubirEntrega").disabled = false;
        
    }
    else 
    {
        document.querySelector("#btnSubirEntrega").disabled = true;
        document.querySelector("#devolucionEntregarTarea").innerHTML = "No hay tareas para  entregar"
    }
}

function DivMostrarDevolucionesDocente(){
    OcultarDivsInterfazDocente()
    let tareasaCorregir=0
    document.querySelector("#DevolucionesTareas").style.display="block";
    document.querySelector("#SeleccionarTareas").innerHTML="";
    for (const alumno of UsuarioActual.alumnos){ //Recorro cada alumno del docente actual
        for (const entrega of alumno.entregas){ //Recorro cada tarea de sus alumnos
            if (!entrega.tareaCorregida){ //Si la tarea no fue corregida la muestro y el nombre del alumno
                tareasaCorregir++
                document.querySelector("#SeleccionarTareas").innerHTML+="<Option value="+entrega.identificador+">"+entrega.tareaOriginal.titulo+"-"+alumno.usuario+"</option>"
            }
        }
    }
    if (tareasaCorregir>0){
        CambiarAudioEntregas()
    }
}

function MostrarDivDevolucionDocente(){ //El alumno ve las devoluciones del docente, si no hay devolucion muestra que esa entrega no tiene devolucion
    OcultarDivsInterfazAlumno()
    let TareasCorregidas=0
    document.querySelector("#VerDevoluciondeDocente").style.display="block"
    document.querySelector("#SeleccionarEntrega").innerHTML=""
    document.querySelector("#ParrafoNohaytareas").innerHTML=""
    for (const entrega of UsuarioActual.entregas){
        if (entrega.tareaCorregida){
            TareasCorregidas++
            document.querySelector("#SeleccionarEntrega").innerHTML+="<Option value="+entrega.identificador+">"+entrega.tareaOriginal.titulo+"</option>"
        }
    }
    if (TareasCorregidas==0) {
        document.querySelector("#ParrafoNohaytareas").innerHTML="Aún no hay tareas corregidas"
    }
}

function MostrarDivTareasResueltas(){
    OcultarDivsInterfazAlumno()
    document.querySelector("#VerTareasResueltas").style.display="block"
    document.querySelector("#ParrafoTareasResueltas").innerHTML=""
    for (const entrega of UsuarioActual.entregas){
            let corregida = ""
            if(entrega.tareaCorregida)
            {
                corregida = "Corregida"
            }
            else 
            {
                corregida = "No corregida"
            }
            document.querySelector("#ParrafoTareasResueltas").innerHTML+="<li>"+entrega.tareaOriginal.titulo+" - "+corregida+"</li>"
    }

    if (UsuarioActual.entregas.length == 0)
    {
        document.querySelector("#ParrafoTareasResueltas").innerHTML="<p>No se a entregado ninguna tarea</p>"
    }
}
//#endregion

// ZONA FUNCIONES REGISTRO O INGRESO

//#region Registro Alumno

function RegistrarAlumno(){

    let nombre =  document.querySelector("#inputANombre").value; //Guardo el nombre que ingreso el usuario

    let docente= document.querySelector("#inputADocente").value; 

   let usuario =  document.querySelector("#inputAUsuario").value; //guardo el usuario que ingreso el usuario
   usuario = usuario.toLowerCase(); //asi o importa si es minusculas o mayusculas y no es case sensitive

   let pass =  document.querySelector("#inputAPass").value; //guardo la contraseña que ingreso el usuario

   document.querySelector("#devolucionARegistro").innerHTML = "" //limpio la devolucion
   error = ""; //pongo el error en blanco



   let casillavacia = EstaVacio(nombre) || EstaVacio(usuario) || EstaVacio(pass) || EstaVacio(docente) //guardo en casilla vacia si alguno de los inputs esta en blanco

   if (casillavacia) // si esta en blanco devuelvo el error
   {
        document.querySelector("#devolucionARegistro").innerHTML = error;
   }
   else //si no esta en blanco compruebo que la contraseña se correcta
   {
        if (!CumplePassword(pass)) //si la constraseñña es incorrecta entonces :
        {
            document.querySelector("#devolucionARegistro").innerHTML = error;
        }
        else // si cumple entonces:
        {
            let existeotrousuarioigual = false; // inicio si existe otro usuario como falso
            for (const alumno of AlumnosRegistrados) { // voy usuario por usuario chequiando si tienen el mismo usuario
                if(alumno.usuario == usuario)
                {
                    existeotrousuarioigual = true; // si encuentro otro con el mismo usuario convierto en verdadero
                    error = "Existe un usuario con el mismo nombre"
                }
            }
            if(existeotrousuarioigual) // si existe otro usuario con el mismo usuario devuelvo el error
            {
                document.querySelector("#devolucionARegistro").innerHTML = error;
            }
            else //si no habia otro usuario con el mismo usuario ya pase todas las pruebas
            {
                let DocenteAsignar=null; // cree una variable // pq quiero que exista fuera de las llaves esas sino solo lo valdria dentro de ese for
                // es exterior del for solo funciona dentro del else 
                for(const docenteDeLista of DocentesRegistrados){ //docente de lista por cada vuelta del for toma la forma de cada docente registrado
                    if(docenteDeLista.usuario==docente) { //cuando el usuario del docente sea el mismo que el elegido en el select del alumno este if sera verdadero

                        // pongo docenteDeLista.usuario le pregunto el usuario que tiene y lo comparo con el que selecciono en el registro de alumno
                        DocenteAsignar= docenteDeLista;


                    }
                }
                let nuevoAlumno = new Alumno(nombre,pass,usuario,DocenteAsignar) // creo el docente nuevo
                AlumnosRegistrados.push(nuevoAlumno); // lo agrego a la lista de docentes registrados
                DocenteAsignar.alumnos.push(nuevoAlumno) //remplazable por funcion de alumno AsignarmeADocente()

                document.querySelector("#inputANombre").value = ""
                document.querySelector("#inputAUsuario").value = ""
                document.querySelector("#inputAPass").value = ""
                document.querySelector("#devolucionARegistro").innerHTML = "Registro Exitoso"
            }
        }
   }

}




//#endregion 

//#region Registro Docente

function RegistrarDocente()
{
   let nombre =  document.querySelector("#inputDNombre").value; //Guardo el nombre que ingreso el usuario

   let usuario =  document.querySelector("#inputDUsuario").value; //guardo el usuario que ingreso el usuario
   usuario = usuario.toLowerCase(); //asi o importa si es minusculas o mayusculas y no es case sensitive

   let pass =  document.querySelector("#inputDPass").value; //guardo la contraseña que ingreso el usuario

   document.querySelector("#devolucionDRegistro").innerHTML = "" //limpio la devolucion
   error = ""; //pongo el error en blanco

   let casillavacia = EstaVacio(nombre) || EstaVacio(usuario) || EstaVacio(pass) //guardo en casilla vacia si alguno de los inputs esta en blanco

   if (casillavacia) // si esta en blanco devuelvo el error
   {
        document.querySelector("#devolucionDRegistro").innerHTML = error;
   }
   else //si no esta en blanco compruebo que la contraseña se correcta
   {
        if (CumplePassword(pass)) //si la constraseñña es correcta continuo
        {
            let existeotrousuarioigual = false; // inicio si existe otro usuario como falso
            for (const docente of DocentesRegistrados) { // voy usuario por usuario chequiando si tienen el mismo usuario
                if(docente.usuario == usuario)
                {
                    existeotrousuarioigual = true; // si encuentro otro con el mismo usuario convierto en verdadero
                    error = "Existe un usuario con el mismo nombre"
                }
            }
            if(existeotrousuarioigual) // si existe otro usuario con el mismo usuario devuelvo el error
            {
                document.querySelector("#devolucionDRegistro").innerHTML = error;
            }
            else //si no habia otro usuario con el mismo usuario ya pase todas las pruebas
            {
                let nuevoDocente = new Docente(nombre,pass,usuario) // creo el docente nuevo
                DocentesRegistrados.push(nuevoDocente); // lo agrego a la lista de docentes registrados
                document.querySelector("#inputDNombre").value = ""
                document.querySelector("#inputDUsuario").value = ""
                document.querySelector("#inputDPass").value = ""
                document.querySelector("#devolucionDRegistro").innerHTML = "Registro Exitoso"
            }
        }
        else // si la constraseña no es correcta devuelvo el error
        {
            document.querySelector("#devolucionDRegistro").innerHTML = error;
        }
   }

}


function EstaVacio(texto)
{
    let devolucion = false;
    if (texto == "")
    {
        devolucion = true;
        error = "Una casilla esta vacia"
    }
    return devolucion;
}

function CumplePassword(password)
{
    let devolucion = true;
    let numerodemayusculas = 0;
    let numerodeminusculas = 0;
    let numerodenumeros = 0;
    if (password.length < 4)
    {
        devolucion = false;
        error = "La constraseña tiene menos de 4 caracteres <br>"
    }
    for (let index = 0; index < password.length; index++) { // voy letra por letra de la password
        let letra = password.charAt(index); //guardo la letra
        if (letra.charCodeAt(0) >= "A".charCodeAt(0) && letra.charCodeAt(0) <= "Z".charCodeAt(0)) //chequeo si es mayuscula
        {
            numerodemayusculas++ //sumo uno a mayusculas
        }
        else if(letra.charCodeAt(0) >= "a".charCodeAt(0) && letra.charCodeAt(0) <= "z".charCodeAt(0))
        {
            numerodeminusculas ++; //sumo una minuscula
        }
        else if(!isNaN(parseInt(letra)))
        {
            numerodenumeros++;
        }
        
    }
    
    if(numerodemayusculas == 0) //Si enncuentra almenos una mayuscula el numero es diferente de 0 entonces no devuelve falso
    {
        devolucion = false;
        error += "La constraseña requiere al menos una mayuscula <br>"
    }
    
    if (numerodeminusculas == 0)
    {
        devolucion = false;
        error += "La constraseña requiere al menos una minuscula <br>"
    }
    
    if (numerodenumeros == 0)
    {
        devolucion = false;
        error += "La constraseña requiere al menos un numero <br>"
    }
    
    return devolucion;
    
    
}

//#endregion

//#region Funcion Ingreso de usuarios

function IngresarUsuario()
{
    document.querySelector("#devolucionLogin").innerHTML = "" //limpio la devolucion del login

    let usuario = document.querySelector("#inputUsuario").value; //guardo el usuario ingresado
    usuario = usuario.toLowerCase(); //para chequear el usuario sin importar las mayusculas o minusculas

    let pass = document.querySelector("#inputPass").value; //guardo la contraseña ingresada

    let nohaycoincidencia = true; //creo una variable de que no hay coincidencia en el usuario

    for (const Docente of DocentesRegistrados) { //busco coincidencias en los docentes 
        if (Docente.usuario == usuario) // si el usuario es igual a el ingresado paso a ver si la constraseña es correcta
        {
            if(Docente.contraseña == pass) // si la constraseña es igual entro al if
            {
                nohaycoincidencia = false; //hay coincidencias
                UsuarioActual = Docente;    //el usuario actual es el docente que tiene el mismo usuario y contraseña
                LoginDocente(); //llamo la funcion de que entro un docente
                document.querySelector("#textoBienvenida").innerHTML = "Bienvenido " + UsuarioActual.nombre
            }
        }
    }

    for (const Alumno of AlumnosRegistrados) { //busco coincidencias con los alumnos
        if (Alumno.usuario == usuario) //si encuentro un alumno con el mismo usuario que el ingresado entro al if
        {
            if(Alumno.contraseña == pass) //si la contraseña coincide ingreso al if
            {
                nohaycoincidencia = false; //hay coincidencia
                UsuarioActual = Alumno; //el usuario actual es el alumno con mismo usuario y pass
                LoginAlumno(); //llamo a la funcion de que entro un alumno
                document.querySelector("#textoBienvenida").innerHTML = "Bienvenido " + UsuarioActual.nombre

            }
        }
    }

    if (nohaycoincidencia) //si no hay coincidencia
    {
        document.querySelector("#devolucionLogin").innerHTML = "Usuario y/o contraseña incorrectos" //devuelvo el error
    }

    document.querySelector("#inputUsuario").value = "";
    document.querySelector("#inputPass").value = "";
}



function LoginDocente()
{
    OcultarDivsMenuInicial(); //Oculto todos los divs
    document.querySelector("#botonesLogYRegistro").style.display = "none"; //oculto el menu de registro y login
    document.querySelector("#divMenuDocente").style.display = "block"; //Muestro 
    
}

function LoginAlumno(){
    OcultarDivsMenuInicial();
    document.querySelector("#botonesLogYRegistro").style.display="none";
    document.querySelector("#divMenuAlumno").style.display = "block";
}

//#endregion

// ZONA FUNCIONES DOCENTE

//#region Docente Subir Tareas


function SubirTarea()
{
    let titulo = document.querySelector("#inputTitulo").value; // GUardo el titulo ingresado

    let descripcion = document.querySelector("#inputDescripcion").value; //guardo la descripcion ingresada

    let img = document.querySelector("#inputImagen").value; //guardo la img elegida

    let nivel = document.querySelector("#inputNivel").value; //guardo el nivel elegido

    error = ""; //vacio el error
    document.querySelector("#devolucionTarea").innerHTML = ""; //vacio la devolucion

    let inputvacio = EstaVacio(titulo) || EstaVacio(descripcion) //guardo si uno de los inputs esta vacio

    if (inputvacio) //si el input esta vacio devuelvo el error
    {
        document.querySelector("#devolucionTarea").innerHTML = error;
    }
    else //si no hay input vacio continuo
    {
        if(titulo.length+descripcion.length <= 20) //si la suma de caracteres es menor a 20 devuelvo error
        {
            document.querySelector("#devolucionTarea").innerHTML = "La suma de caracteres de la descripcion y el titulo debe ser mayor a 20"
        }
        else //si no continuo
        {
            if(titulo.length+descripcion.length > 200) //si la suma de caracteres es mayor a 200 devuelvo error
            {
                document.querySelector("#devolucionTarea").innerHTML = "La suma de caracteres de la descripcion y el titulo debe ser menor a 200"
            }
            else // si no continuo
            {
                let nuevaTarea = new Tarea(nivel,img,descripcion,titulo); //creo una nueva tarea y le asigno sus valores iniciales
                UsuarioActual.tareas.push(nuevaTarea); //agrego la tarea a la lista de tareas del docente actual

                document.querySelector("#inputTitulo").value = "" //limpio el input de titulo
                document.querySelector("#inputDescripcion").value = "" //limpio el input de descripcion
            }
        }
    }
}

function MostrarIMG()
{
    let img = document.querySelector("#inputImagen").value; //guardo la img elegida

    document.querySelector("#imgPreview").src = "img/"+img; // agarro la imagen seleccionada y la coloco
}




//#endregion

//#region Devolucion Docente
function HacerDevolucionEntrega(){
    let Devolucion=document.querySelector("#inputDevolucion").value; //La devolución será el valor del textarea
    let EntregaSeleccionada=document.querySelector("#SeleccionarTareas").value;
    for (const alumno of UsuarioActual.alumnos){
        for (const entrega of alumno.entregas){
            if (EntregaSeleccionada==entrega.identificador){
                entrega.devolucion=Devolucion //La devolucion de la entrega será igual a lo que puso el docente en el textarea
                entrega.tareaCorregida=true //La tarea ahora está corregida
            }
        }
    }
    document.querySelector("#inputDevolucion").value=""
    DivMostrarDevolucionesDocente()
}


function CambiarAudioEntregas(){
    let EntregaSeleccionada=document.querySelector("#SeleccionarTareas").value;
    for (alumno of UsuarioActual.alumnos){
        for (entrega of alumno.entregas){
            if (EntregaSeleccionada==entrega.identificador){
                document.querySelector("#ReproductorEntregas").src="audio/"+entrega.audio; 
                document.querySelector("#ReproductorEntregas").load()
            }
        }
    }
}

//#endregion

//#region Subir Nivel de alumnos

function SubirDeNivelaAlumnos(){
    let AlumnoSeleccionado=document.querySelector("#SeleccionarNivel").value; //Defino el alumno seleccionado en el select
    for (const alumno of UsuarioActual.alumnos){ //Recorro cada alumno de la lista de alumnos del docente actual
        if (alumno.usuario==AlumnoSeleccionado){ //Si algun alumno de esa lista tiene el mismo usuario que el AlumnoSeleccionado
            if (alumno.nivel=="inicial"){ //Chequeo el nivel actual del alumno y lo paso al siguiente nivel
                alumno.nivel="intermedio"
            }
            else {
                if (alumno.nivel=="intermedio"){
                    alumno.nivel="avanzado"
                }
                else {
                    if (alumno.nivel=="avanzado"){ //Si el nivel actual del alumno es avanzado, ya no puedo subir más, aviso un error
                        document.querySelector("#DevolucionNivel").innerHTML="El alumno ya está en nivel máximo"
                    }
                }
            }
        }
    }
    DivMostrarNivelDocente()
}

//#endregion

//#region Funcion Estadisticas Docente

function EstadisticasDocente(){
    OcultarDivsInterfazDocente()
    document.querySelector("#EstadisticasD").style.display = "block";
    document.querySelector("#SeleccionarAlEstadisticas").innerHTML = "";
    document.querySelector("#EstadisticasD1").innerHTML="";
    document.querySelector("#EstadisticasD2").innerHTML="";
    document.querySelector("#EstadisticasD3").innerHTML = "";

    //Armar select
    for (const alumno of UsuarioActual.alumnos) {
        document.querySelector("#SeleccionarAlEstadisticas").innerHTML+="<Option value="+alumno.usuario+">"+alumno.nombre+"-"+alumno.nivel+"</option>"
    }

    let resultado="";
    let cantidadMaxima=-1;
    let suma=0;
    let hayError=false;

    for(let unAlumno of UsuarioActual.alumnos){
        let cantidad= unAlumno.entregas.length; // me va decir cuantos entregas tuvo el alumno
        suma= suma+cantidad; // cantidad de ejercicios entregados
        if(cantidad>cantidadMaxima){
            cantidadMaxima=cantidad;
            resultado=unAlumno.nombre;
        }
        else if(cantidad==cantidadMaxima){
            resultado+=" "+ unAlumno.nombre + ",";

        }
    }
   
    if(UsuarioActual.tareas.length == 0)
    {
        document.querySelector("#EstadisticasD1").innerHTML+= "No hay tareas propuestas por este docente por lo tanto no se muestran estadisticas, <br>"
        hayError=true
    }
    if(UsuarioActual.alumnos.length == 0)
    {
        document.querySelector("#EstadisticasD1").innerHTML+= "No hay alumnos registrados a este docente por lo tanto no se muestran estadisticas, <br>"
        hayError=true
    }
    if (suma == 0)
    {
        document.querySelector("#EstadisticasD1").innerHTML+= "No hay entregas para este docente por lo tanto no se muestran estadisticas,, <br>"
        hayError=true
    }
    if (!hayError)
    {
        document.querySelector("#EstadisticasD1").innerHTML="El o los alumnos con mas entregas son: "+resultado+ " con : "+cantidadMaxima + " ejercicios resueltos";
        document.querySelector("#EstadisticasD2").innerHTML="La cantidad de ejercicios resueltos en total son : "+suma;
    }

    


   

}

function MostrarEstadisticasAlumno(){

    let alumnoSeleccionado = document.querySelector("#SeleccionarAlEstadisticas").value;
    let alumnoestadistica = null

    for (const alumno of UsuarioActual.alumnos) {
        if (alumno.usuario == alumnoSeleccionado)
        {
            alumnoestadistica = alumno;
        }
    }

    let divEstadisticaAl = document.querySelector("#EstadisticasD3")
    divEstadisticaAl.innerHTML = "";
    for (const tarea of UsuarioActual.tareas) {
        if(tarea.nivel == alumnoestadistica.nivel)
        {
            let resuelta = false
            for (const alumno of UsuarioActual.alumnos) {
                for (const entrega of alumno.entregas) {
                    if (tarea.titulo == entrega.tareaOriginal.titulo)
                    {
                        resuelta = true;
                    }
                }
            }
            if (resuelta)
            {
                divEstadisticaAl.innerHTML += "<p>"+tarea.titulo+" - "+"Entregada"+"</p>"
            }
            else 
            {
                divEstadisticaAl.innerHTML += "<p>"+tarea.titulo+" - "+"No Entregada"+"</p>"
            }
            
        }
    }

}

//#endregion

// ZONA FUNCIONES ALUMNO

//#region Alumno Subir Entregas

function EntregarTareas()
{
    let audio = document.querySelector("#SeleccionarAudios").value; // el audio que elegi en el select

    let titulotarea = document.querySelector("#SeleccionarTarea").value; //el titulo que elegi en el select

    let tareaaentregar = null; //la tarea que estoy entregando

    for (const tarea of UsuarioActual.docente.tareas) { //Por cada tarea del docente del alumno actual
        if(tarea.titulo == titulotarea) //si la tarea del docente es igual a la elegida en el select entro
        {
            tareaaentregar = tarea; //la tarea a entregar es esta tarea
        }
    }

    let entrega = new Entrega(audio,tareaaentregar,ContadorEntregas) //creo una entrega con el audio y con la tarea que estoy entregando y con el identificador contadorentregas
    ContadorEntregas++ //sumo 1 al contador global de entregas
    UsuarioActual.entregas.push(entrega) //sumo la entrega a la lista de entregas de este alumno

    DivMostrarEntregaAlumno(); //Recargo el div de entregas para dejar de mostrar titulo y imagen de una tarea ya resuelta

}

//#endregion

//#region Alumno Ver Tareas

function MostrarTareas() // esto se llama para mostrar una tarea basada en el select de tareas
{

    let titulotarea = document.querySelector("#verSeleccionarTarea").value; //titulo de la tarea pero sin espacios en blanco

    let tareaactual = null;

    for (const tarea of UsuarioActual.docente.tareas) { //Por cada tarea del docente del alumno actual
        if(tarea.titulo == titulotarea) //si la tarea del docente (sin espacios en blanco) es igual a la del select que ya no tiene espacios en blanco
        {
            tareaactual = tarea; //esa es la tarea que voy a usar para generar la info de la tarea que se va a mostrar
        }
    }
    

    document.querySelector("#verEntregaTituloMostrar").innerHTML = tareaactual.titulo;
    document.querySelector("#verDescripcionMostrar").innerHTML = tareaactual.descripcion;
    document.querySelector("#verImagenMostrar").src = "img/"+tareaactual.imagen;


}

function BuscarTarea()
{
    let resultadosConTitulos = 0;
    let resultadosConDescripciones = 0;
    let htmlCompleto = "";
    let textoBusqueda = document.querySelector("#inputBuscarTarea").value;
    textoBusqueda = textoBusqueda.toLowerCase();
    for (const tarea of UsuarioActual.docente.tareas) {
        if(tarea.nivel == UsuarioActual.nivel)
        {
            let titulodetarea = tarea.titulo.toLowerCase();
            if(titulodetarea.includes(textoBusqueda))
            {
                htmlCompleto += ArmarElementoTarea(tarea);
                resultadosConTitulos++;
            }
        }
    }
    //Busco en las descripciones si no hay resultados con titulo
    if (resultadosConTitulos == 0)
    {
        for (const tarea of UsuarioActual.docente.tareas) {
            if(tarea.nivel == UsuarioActual.nivel)
            {
                let descripciondetarea = tarea.descripcion.toLowerCase();
                if(descripciondetarea.includes(textoBusqueda))
                {
                    htmlCompleto += ArmarElementoTarea(tarea);
                    resultadosConDescripciones++;
                }
            }
        }
    }

    document.querySelector("#listaBuscarTarea").innerHTML = htmlCompleto
    if(resultadosConDescripciones == 0 && resultadosConTitulos == 0)
    {
        document.querySelector("#listaBuscarTarea").innerHTML = "<p>No se han encontrado tareas que coincidan con su busqueda</p>"
    }
}

function ArmarElementoTarea(tarea)
{
    let htmlRetorno = ""
    htmlRetorno += "<h2>"+tarea.titulo+"</h2>"
    htmlRetorno += "<br><br>"
    htmlRetorno += "<img src='img/"+tarea.imagen+"' width='600' height='400'  style='border: 5px solid #214F4B'>"
    htmlRetorno += "<br><br>"
    htmlRetorno += "<h3>"+tarea.descripcion+"</h3>"
    htmlRetorno += "<br><br><hr>"
    return htmlRetorno;
}

//#endregion

//#region Alumno Ver devolucion docente
function VerDevolucionDocente(){
    let EntregaSeleccionada=document.querySelector("#SeleccionarEntrega").value;
    for (const entrega of UsuarioActual.entregas){ //por cada entrega del alumno actual
        if (entrega.tareaCorregida){ //si la tarea fue corregida
            if (EntregaSeleccionada==entrega.identificador) //la entrega del select es igual
            document.querySelector("#ParrafoDevolucion").innerHTML=entrega.devolucion
        }
    }
}
//#endregion

//#region Funciones Estadisticas Alumno

function EstadisticasAlumno(){

    OcultarDivsInterfazAlumno();
    document.querySelector("#EstadisticasA").style.display = "block";
    document.querySelector("#EstadisticasA1").innerHTML=""
    document.querySelector("#EstadisticasA2").innerHTML=""
    
   let cantidadCorregidas=0;
   let cantidadNoCorregidas=0;
   let porcentajeCorregidos=0;
   let hayError=false;

    for(let entrega of UsuarioActual.entregas){
       if(entrega.tareaCorregida){
           cantidadCorregidas++;
       }
       else{
            cantidadNoCorregidas++;
       }
       
    }

    if(UsuarioActual.docente.tareas.length == 0)
    {
        document.querySelector("#EstadisticasA1").innerHTML+= "No hay tareas propuestas por este docente por lo tanto no se muestran estadisticas, <br>"
        hayError = true;
    }
    if(UsuarioActual.entregas.length == 0)
    {
        document.querySelector("#EstadisticasA1").innerHTML+= "No ha realizado ninguna entrega, no hay estadisticas disponibles, <br>"
        hayError = true;
    }
    if (cantidadCorregidas == 0)
    {
        document.querySelector("#EstadisticasA1").innerHTML+= "No se ha corregido ninguna entrega, no hay estadisticas disponibles, <br>"
        hayError = true;
    }
    if(!hayError)
    {
        porcentajeCorregidos= UsuarioActual.entregas.length*100/(UsuarioActual.docente.tareas.length);
        porcentajeCorregidos = porcentajeCorregidos.toFixed(2); //hago que tenga 2 numeros despues de la coma

        document.querySelector("#EstadisticasA1").innerHTML="Cantidad de ejercicios corregidos "+cantidadCorregidas+ "<br>Cantidad de ejercicios no corregidos: "+cantidadNoCorregidas;

        document.querySelector("#EstadisticasA2").innerHTML="Ejercicios corregidos : " + porcentajeCorregidos + "%, " + UsuarioActual.entregas.length+"/"+UsuarioActual.docente.tareas.length;
    }

    
}

//#endregion

// ZONA FUNCIONES MIXTAS

//#region Funcion Cerrar Sesion
function Salir(){
    OcultarDivsMenuInicial()
    OcultarDivsInterfazAlumno()
    OcultarDivsInterfazDocente()
    document.querySelector("#botonesLogYRegistro").style.display = "block";
    UsuarioActual=null; 
    document.querySelector("#textoBienvenida").innerHTML = "Bienvenido "
}
//#endregion

