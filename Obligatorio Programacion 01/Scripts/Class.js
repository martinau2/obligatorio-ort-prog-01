class Docente
{
    constructor(pNombre,pContraseña,pUsuario)
    {
        this.nombre=pNombre
        this.contraseña=pContraseña
        this.usuario=pUsuario 
        this.alumnos=[]
        this.tareas=[]

    }
}

class Alumno
{
    constructor(pNombre,pContraseña,pUsuario,pDocente)
    {
        this.nombre=pNombre
        this.contraseña=pContraseña
        this.usuario=pUsuario 
        this.docente= pDocente
        this.entregas=[]
        this.nivel = "inicial"

    }

    AsignarmeADocente()
    {
        if(!this.docente.alumnos.includes(this))
        {
            this.docente.alumnos.push(this)
        }
    }
}

class Tarea
{
    constructor(pNivel,pImagen,pDescripcion,pTitulo)
    {
        this.nivel = pNivel
        this.descripcion = pDescripcion
        this.titulo = pTitulo
        this.imagen = pImagen
    }
}

class Entrega
{
    constructor(pAudio,pTareaOriginal,pIdentificador)
    {
        this.tareaOriginal = pTareaOriginal
        this.audio = pAudio
        this.tareaCorregida = false
        this.devolucion = ""
        this.identificador = pIdentificador
    }
}